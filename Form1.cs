﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        SqlConnections sqlperson = new SqlConnections("person");
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dispData();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e) // insert
        {
            sqlperson.Insert(textBox1.Text, textBox2.Text, textBox3.Text);
            dispData();
        }

        private void button3_Click(object sender, EventArgs e) //update
        {
            /*sqlCon.Open();
            SqlCommand cmd = sqlCon.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "update person set name ='" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "')";
            cmd.ExecuteNonQuery();
            sqlCon.Close();
            //MessageBox.Show("recorded data");*/
            dispData();
        }

       

        private void button2_Click(object sender, EventArgs e) // delete
        {
            sqlperson.Delete(textBox1.Text);
            dispData();

        }

        private void button4_Click(object sender, EventArgs e) // display
        {
            dispData();
        }


        public void dispData()
        {
            dataGridView1.DataSource = sqlperson.PullData();
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();

        }
    }
}
