﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class SqlConnections
    {
        string tablename = "";

        SqlConnection sqlCon = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\clip\Documents\testing.mdf;Integrated Security=True;Connect Timeout=30");
        
        
        
        
        
        
        
        public SqlConnections(string tablename)
        {
            this.tablename = tablename;
        }
       

        public void Insert(string box1, string box2, string box3)
        {
            try
            {
                sqlCon.Open();                                                                                                     //tryes to open the connetion to the SQL server
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            string table = string.Format("INSERT INTO {0} (name,city,contry) ", this.tablename);
            SqlCommand myCommand = new SqlCommand( table + "Values (@textBox1,@textBox2,@textBox3)", sqlCon);                   // Makeing SQL quarry
            myCommand.Parameters.AddWithValue("@textBox1", box1);
            myCommand.Parameters.AddWithValue("@textBox2", box2);
            myCommand.Parameters.AddWithValue("@textBox3", box3);
            myCommand.ExecuteNonQuery();                                                                                        //Sending the quarry
            try
            {
                sqlCon.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void Update()
        {

        }

        public void Delete(string box1)
        {
            try
            {
                sqlCon.Open();                                                                                                  //tryes to open the connetion to the SQL server
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            string table = string.Format("delete from {0} where name =", this.tablename);
            SqlCommand myCommand = new SqlCommand(table + "@textBox1", sqlCon);                         // Makeing SQL quarry
            myCommand.Parameters.AddWithValue("@textBox1", box1);
            myCommand.ExecuteNonQuery();                                                                                        //Sending the quarry
            try
            {
                sqlCon.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public DataTable PullData()
        {
            sqlCon.Open();
            SqlDataReader myReader = null;
            SqlCommand myCommand = new SqlCommand("select * from person", sqlCon);
            myReader = myCommand.ExecuteReader();
            DataTable dt = new DataTable();
            while (!myReader.IsClosed)
            {
                
                // DataTable.Load automatically advances the reader to the next result set
                dt.Load(myReader);
                dt.Rows.Add(dt);
            }
            sqlCon.Close();
            return dt;


        }

    }
}
